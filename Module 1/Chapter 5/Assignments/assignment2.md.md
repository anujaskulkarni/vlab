# Data Structure
# Introduction

To understand how various data structures work. To understand some important applictions of various data structures. To familiarize how certain applications can benefit from the choice of data structures. To understand how the choice of data structures can lead to efficient implementations of algorithms.
<b>
### List of Experiments</b>
<!---Links--->
> 1.[Number Systems](http://cse01-iiith.vlabs.ac.in/exp1/Introduction.html?domain=Computer%20Science&lab=Data%20Structures)<br>
2.[Expression Evaluation using Stacks](http://cse01-iiith.vlabs.ac.in/exp2/Introduction.html?domain=Computer%20Science&lab=Data%20Structures)<br>
3.[Sorting using Arrays](http://cse01-iiith.vlabs.ac.in/exp3/Introduction.html?domain=Computer%20Science&lab=Data%20Structures)<br>
4.[Polynomials via Linked Lists](http://cse01-iiith.vlabs.ac.in/exp3/Introduction.html?domain=Computer%20Science&lab=Data%20Structures)<br>
5.[Search Trees](http://cse01-iiith.vlabs.ac.in/exp5/Introduction.html?domain=Computer%20Science&lab=Data%20Structures)<br>
6.[Expression Trees](http://cse01-iiith.vlabs.ac.in/exp7/Introduction.html?domain=Computer%20Science&lab=Data%20Structures)<br>
7.[Graph Traversals](http://cse01-iiith.vlabs.ac.in/exp7/Introduction.html?domain=Computer%20Science&lab=Data%20Structures)<br>
8.[Shortest Paths in Graphs](http://cse01-iiith.vlabs.ac.in/exp7/Introduction.html?domain=Computer%20Science&lab=Data%20Structures)<br>
9.[Minimum Spanning Trees](http://cse01-iiith.vlabs.ac.in/exp9/Introduction.html?domain=Computer%20Science&lab=Data%20Structures) 

#### Explore New Data Structure Labs

Take a look at our newly developed Data Structures Labs enriched with detailed concept explanations through videos, simulated demos, practice and exercises.
<!--ol--->
1.[Data Structures Lab - I](https://ds1-iiith.vlabs.ac.in/data-structures-1/)<br>
2.[Data Structures Lab - II](https://ds2-iiith.vlabs.ac.in/data-structures-2/)
<!----Image----->

![Markdown Logo](http://markdown-here.com/img/icon256.png)

